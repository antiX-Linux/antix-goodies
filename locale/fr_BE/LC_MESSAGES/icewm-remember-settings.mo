��          �      ,      �  6   �      �  �   �     �     �     �  T   �  w   B     �     �     �  U   �     )  <   /  ;   l  	   �  �  �  D   b  B   �  T  �     ?     K     P  |   W  �   �     {	     �	     �	  n   �	     
  H   
  S   a
     �
                                  	                
                            <b>Select a program. Store it's window properties.</b> Add/Remove IceWM Window Defaults Entries shown below are for the <b>$appclass</b> ($appname) window.\n\nAll options marked will be saved, all unmarked will be deleted.\n\n Note: Workspace number shown is the window's current workspace. \n 	Don't worry that it appears too low.\n\n Geometry HELP Layer Next time you launch the program, it will remember the window properties last saved. Save the <b>size and position</b>, <b>workspace</b> and <b>layer</b> of a window using the IceWM-remember-settings app. Select Select other Type Use the <b>Select other</b> option to select a different window/program to configure. Value What window configuration you want antiX to remember/forget? You can also delete this information unticking all options. workspace Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:49+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (Belgium) (https://app.transifex.com/anticapitalista/teams/10162/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 <b>Choisir un programme. Sauver les propriétés de sa fenêtre.</b> Ajouter/Supprimer les paramètres par défaut de la fenêtre IceWM Les entrées ci-dessous concernent la fenêtre <b>$appclass</b> ($appname).\n\nToutes les options marquées seront enregistrées, toutes les non marquées seront supprimées.\n\n Remarque: le numéro de l’espace de travail indiqué est l’espace de travail actuel de la fenêtre. \n 	Ne vous inquiétez pas si elle semble trop basse.\n\n Géométrie AIDE Couche La prochaine fois que vous lancerez le programme, il se souviendra des dernières propriétés de la fenêtre enregistrées. Enregistrer la <b>taille et la position</b>, <b>l’espace de travail</b> et <b>la couche</b> d’une fenêtre à l’aide de l’application IceWM-remember-settings. Sélectionner Sélectionner autre Type Utilisez l’option <b>Sélectionner autre</b> pour sélectionner un(e) autre fenêre/programme à configurer. Valeur Quelle configuration de fenêtre voulez-vous qu’antiX retienne/oublie? Vous pouvez également supprimer ces informations en décochant toutes les options. espace de travail 