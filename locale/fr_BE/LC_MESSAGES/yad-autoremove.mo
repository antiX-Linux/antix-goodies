��          \      �       �      �       �   #     /   +  /   [     �     �  �  �     [  %   y  (   �  3   �  =   �     :  (   I                                       Internet connection detected No Internet connection detected! Waiting for a Network connection... You are Root or running the script in sudo mode You entered the wrong password or you cancelled already root antiX - autoremove Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-12-27 14:16+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (Belgium) (https://app.transifex.com/anticapitalista/teams/10162/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Connexion internet détectée Aucune connexion internet détectée! En attente d’une connexion réseau ... Vous êtes Root ou exécutez le script en mode sudo Vous avez entré le mauvais mot de passe ou vous avez annulé déjà en root antiX autoremove suppression automatique 