��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m    �  -   �  /   �  !   �  +     '   I     q     �     �     �     �     �     �  #   �                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-29 21:26+0000
Last-Translator: c1ddf36d8d5bed634b45ccc5d56be96f_99cf421 <c70f93bf335b7f698ba6752fd301b09c_531191>, 2016
Language-Team: French (Belgium) (http://www.transifex.com/anticapitalista/antix-development/language/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 (Connexion au périphérique choisi réussie) (Connexion au périphérique choisi impossible) Carte son par défaut actuelle %s Aucun périphérique ou carte son détecté Une seule carte son a été détectée. Veuillez choisir une carte son Quitter Carte son réglée sur %s Test son Le test a échoué Le test a réussi Test du son pendant 6 secondes Souhaitez-vous tester la carte son? 