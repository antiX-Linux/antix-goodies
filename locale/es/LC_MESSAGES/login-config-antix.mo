��          t      �         
             #     8     J     W     o     }  
   �     �  �  �      A     b  #   j     �     �     �     �     �                                   
                               	    Auto-login Change Change Login Manager Change background Default user Enable numlock at login Login Manager Select Theme Test Theme Test the theme before using it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:03+0000
Last-Translator: Amigo, 2022
Language-Team: Spanish (https://www.transifex.com/anticapitalista/teams/10162/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Iniciar sesión automáticamente Cambiar Cambiar gestor de inicio de sesión Cambiar Fondo de Inicio Usuario predeterminado Activar Bloq Num al iniciar Gestor de inicio de sesión Seleccionar Tema Probar Tema Probar el tema antes de usarlo 