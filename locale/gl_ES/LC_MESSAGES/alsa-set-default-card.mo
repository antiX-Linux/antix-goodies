��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  �  �  4   E  8   z     �  %   �  "   �          :  $   @     e     r     �     �     �                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-29 21:26+0000
Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>, 2020
Language-Team: Galician (Spain) (http://www.transifex.com/anticapitalista/antix-development/language/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 (Foi posible conectarse ao dispositivo seleccionado) (Non foi posible conectarse ao dispositivo seleccionado) A predeterminada actual é %s Non se atoparon tarxetas/dispositivos Só se atopou unha tarxeta de son. Seleccionar unha tarxeta de son Saír Establecida a tarxeta de son para %s Probar o son Proba fallida Proba realizada con éxito Probando o son ata 6 segundos Verificar se o son funciona? 