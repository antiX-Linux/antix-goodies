��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  _    =   }     �  *   �  1   �  1        Q     X     k     �  Z   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: Green, 2022
Language-Team: Japanese (https://www.transifex.com/anticapitalista/teams/10162/ja/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ja
Plural-Forms: nplurals=1; plural=0;
 時間帯の選択（カーソルと enter キーを使用） 日付 日付と時刻の設定を管理します スライダを正しい [時] へ移動します スライダを正しい [分] へ移動します 終了 時間帯の選択 現在日時の設定 現在時刻の設定 インターネットタイムサーバを使用して自動的に日時を設定します 