��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  �  �  O   �  Z   �  3   ?  B   s  C   �  *   �     %  9   4     n  &   �  "   �  =   �  X                                	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-29 21:26+0000
Last-Translator: Лазар Миловановић <lazar.milovanovic@outlook.com>, 2019
Language-Team: Serbian (http://www.transifex.com/anticapitalista/antix-development/language/sr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sr
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 (Могао сам да се повежем на изабрани уређај) (Нисам могао сам да се повежем на изабрани уређај) Тренутно је подразумевано %s Звучне карте/уређаји нису пронађени Само једна звучна карта је пронађена Изаберите звучну карту Напусти Звучна карта је постављена на %s Провера звука Провера је неуспешна Провера је успешна Проверавам звук највише 6 секунди Да ли бисте хтели да проверите исправност звука? 