��    	      d      �       �   [   �   B   =  0   �  �   �  G   ?  .   �  W   �  9     �  H  k   2  P   �  :   �  �   *  S   �  (   /  d   X  A   �                                          	    A removable drive with a mounted partition was not found.
It is safe to unplug the drive(s) About to unmount:
$summarylist
Please confirm you wish to proceed. Data is being written to devices.
Please wait... Mountpoint removal failed.

A mountpoint remains present at:
$mountpointerrorlist
Check each mountpoint listed before unpluging the drive(s). Nothing has been unmounted.
Aborting as requested with no action taken. Nothing selected.
Aborting without unmounting. The following are currently mounted:
$removablelist
Choose the drive(s) to be unplugged Unmounted:
$summarylist
It is safe to unplug the drive(s) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-12-15 00:36+0200
PO-Revision-Date: 2019-01-25 15:00+0200
Last-Translator: scootergrisen
Language-Team: Danish (http://www.transifex.com/anticapitalista/antix-development/language/da/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
X-Launchpad-Export-Date: 2012-01-02 10:46+0000
 Der blev ikke fundet noget flytbart drev med en monteret partition.
Det er sikkert at fjerne drevet/drevene Er ved at afmontere:
$summarylist
Bekræft venligst om du ønsker at fortsætte. Data er ved at blive skrevet til enheder.
Vent venligst... Fjernelse af monteringspunkt mislykkedes.

Der er stadig et monteringspunkt på:
$mountpointerrorlist
Tjek hvert monteringspunkt i listen nedenfor inden drevet/drevene fjernes. Intet blev afmonteret.
Afbryder som anmodet uden der blev foretaget nogen handling. Intet valgt.
Afbryder uden at afmontere. På nuværende tidspunkt er følgende monteret:
$removablelist
Vælg drevet/drevene som skal fjernes Afmonterede:
$summarylist
Det er sikkert at fjerne drevet/drevene 