��          |      �          1   !     S  9   Z     �     �  N   �  "   �          6  %   H  �   n  �  Q  (         )  G   /     w     �  U   �  *   �  !        8  (   H  �   q           	   
                                               Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Error. Something went wrong. \n Please check if you entered a valid selection. Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  \n \n Note: you can use your file manager to simply drag file(s) or folder(s) from the Trash Can back to where you want them,\n or do it by manually selecting their number from a list, using the button '$empty_trash_button' \n Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: Dede Carli <dede.carli.drums@gmail.com>, 2023
Language-Team: Italian (https://www.transifex.com/anticapitalista/teams/10162/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Sei sicuro di voler svuotare il cestino? Fatto del tuo spazio su disco.\n Lo spazio libero nella tua cartella home è: Svuota Cestino Svuota cestino Errore. Qualcosa è andato storto. \n Controlla se hai inserito una selezione valida. Ripristina manualmente un file dal cestino Seleziona il file da ripristinare Gestore cestino \n Il tuo cestino sta attualmente usando \n \n Nota: puoi usare il tuo file manager per trascinare semplicemente il/i file o la/e cartella/e dal cestino a dove vuoi,\n o farlo selezionando manualmente il loro numero da un elenco, usando il pulsante '$empty_trash_button' \n 