��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  �  �  ,   *  2   W     �  "   �      �     �            
   +     6     J  )   Y     �                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-29 21:26+0000
Last-Translator: Martin O'Connor <mar.oco@arcor.de>, 2017
Language-Team: German (http://www.transifex.com/anticapitalista/antix-development/language/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 (Konnte mit dem gewählten Gerät verbinden) (Konnte mit dem gewählten Gerät nicht verbinden) Aktueller Standard ist %s Keine Soundkarten/Geräte gefunden Eine einzige Soundkarte gefunden Wählen Sie bitte Soundkarte Beenden Soundkarte auf %s eingestellt Sound-Test Test fehlgeschlagen Test bestanden Der Sound wird bis zu 6 Sekunden getestet Möchten Sie den Sound testen? 