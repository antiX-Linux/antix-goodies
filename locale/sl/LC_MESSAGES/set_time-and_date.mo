��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  �    >   �       &     $   F  '   k     �     �     �     �  J   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2021
Language-Team: Slovenian (https://www.transifex.com/anticapitalista/teams/10162/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 Izberite časovni pas (s pomočjo smernih tipk in tipke enter) Datum: Upravljanje nastavitev datuma in časa Premaknite drsnik za popravljanje ur Premaknite drsnik za popravljanje minut Končaj Izbira časovnega pasu Nastavi trenutni datum Nastavi trenutni čas Uporabi internetni časovni strežnik za samodejno nastavitev datuma/časa 