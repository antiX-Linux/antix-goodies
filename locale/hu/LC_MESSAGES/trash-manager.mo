��          |      �          1   !     S  9   Z     �     �  N   �  "   �          6  %   H  �   n  f  Q  $   �     �  J   �     .     >  Z   N  -   �  (   �                          	   
                                               Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Error. Something went wrong. \n Please check if you entered a valid selection. Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  \n \n Note: you can use your file manager to simply drag file(s) or folder(s) from the Trash Can back to where you want them,\n or do it by manually selecting their number from a list, using the button '$empty_trash_button' \n Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: Feri, 2023
Language-Team: Hungarian (https://www.transifex.com/anticapitalista/teams/10162/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
   Biztos, hogy kiüríti a kukát?   Kész területet használ a lemezen.\nA sajátkönyvtárában a szabad terület: Kuka ürítése Kuka ürítése Hiba: Valami hiba történt.\n Ellenőrizze, hogy egy érvényes választást adott-e meg. Fájl manuális helyreállítása a kukából Válassza ki a visszaállítandó fájlt Kukakezelő \n A kuka jelenleg \n \n Megjegyzés: Használhatja a fájlkezelőjét, hogy egyszerűen áthúzzon fájlokat vagy mappákat a Kukából ahová szeretné,\n vagy manuálisan is elvégezheti a számuk kiválasztásával egy listáról, a '$empty_trash_button' gomb használatával \n 