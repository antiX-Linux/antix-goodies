��          t      �         
             #     8     J     W     o     }  
   �     �  �  �     �  
   �  +   �     �       $   *     O     g     �  9   �                         
                               	    Auto-login Change Change Login Manager Change background Default user Enable numlock at login Login Manager Select Theme Test Theme Test the theme before using it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:03+0000
Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>, 2023
Language-Team: Hebrew (Israel) (https://www.transifex.com/anticapitalista/teams/10162/he_IL/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: he_IL
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;
 כניסה אוטומטית החלפה החלפת מנהל כניסה למערכת החלפת רקע משתמש ברירת מחדל הפעלת NumLock עם הכניסה מנהל התחברות בחירת ערכת עיצוב בדיקת ערכת עיצוב בדיקת ערכת העיצוב טרם השימוש בה 