# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Panwar108 <caspian7pena@gmail.com>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-22 20:45+0300\n"
"PO-Revision-Date: 2020-05-22 17:47+0000\n"
"Last-Translator: Panwar108 <caspian7pena@gmail.com>, 2021\n"
"Language-Team: Hindi (https://www.transifex.com/anticapitalista/teams/10162/hi/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hi\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: android-device-usb-connect.sh:12
msgid "Android Device USB Connect"
msgstr "एंड्राइड उपकरण हेतु यूएसबी अभिगम"

#: android-device-usb-connect.sh:18
msgid "Clearing files and preparing directories"
msgstr "फाइल व डायरेक्टरी संरचना करना जारी"

#: android-device-usb-connect.sh:27
msgid "Checking availability of required utilities"
msgstr "आवश्यक साधन हेतु उपलब्धता जाँच जारी"

#: android-device-usb-connect.sh:31
msgid "Error: yad is not available"
msgstr "त्रुटि : yad अनुपलब्ध"

#: android-device-usb-connect.sh:35
msgid "Error: jmtpfs is not available"
msgstr "त्रुटि : jmtpfs अनुपलब्ध"

#: android-device-usb-connect.sh:37
msgid "Checking availability of required utilities finished successfully"
msgstr "आवश्यक साधन हेतु उपलब्धता जाँच सफलतापूर्वक पूर्ण"

#: android-device-usb-connect.sh:43
msgid "An android device seems to be mounted"
msgstr "संभवतः एंड्राइड उपकरण माउंट है"

#: android-device-usb-connect.sh:44
msgid ""
"An android device seems to be mounted.\\n \\nChoose 'Unmount' to unplug it "
"safely OR \\n Choose 'Access device' to view the device's contents again.   "
msgstr ""
"संभवतः एंड्रॉइड उपकरण माउंट है।\\n \\n सुरक्षित रूप से हटाने हेतु 'माउंट से "
"हटाएँ' चुनें या \\n पुनः उपकरण सामग्री देखने हेतु 'उपकरण अभिगम' पर क्लिक "
"करें।"

#: android-device-usb-connect.sh:44
msgid "Access device"
msgstr "उपकरण अभिगम"

#: android-device-usb-connect.sh:44
msgid "Unmount"
msgstr "माउंट से हटाएँ"

#: android-device-usb-connect.sh:46
msgid "User has chosen to access the android device"
msgstr "उपयोक्ता द्वारा एंड्रॉइड अभिगम हेतु अनुरोध"

#: android-device-usb-connect.sh:47
msgid "User has chosen to unmount the android device"
msgstr "उपयोक्ता द्वारा एंड्रॉइड उपकरण माउंट से हटाने हेतु अनुरोध"

#: android-device-usb-connect.sh:50 android-device-usb-connect.sh:51
msgid "Android device WAS NOT umounted for some reason, do not unplug!"
msgstr ""
"अज्ञात कारणवश एंड्रॉइड उपकरण माउंट से हटाना विफल, कृपया उपकरण अभी न हटाएँ!"

#: android-device-usb-connect.sh:51 android-device-usb-connect.sh:54
msgid "OK"
msgstr "ठीक है"

#: android-device-usb-connect.sh:53 android-device-usb-connect.sh:54
msgid "Android device is umounted; it is safe to unplug!"
msgstr "एंड्रॉइड उपकरण माउंट नहीं है; यह उपकरण हटाना सुरक्षित है!"

#: android-device-usb-connect.sh:66
msgid "No device connected"
msgstr "कोई उपकरण कनेक्ट नहीं है"

#: android-device-usb-connect.sh:67
msgid "Device is connected"
msgstr "उपकरण कनेक्ट है"

#: android-device-usb-connect.sh:69
msgid ""
"No (MTP enabled) Android device found!\\n  \\n Connect a single device using"
" its USB cable and \\n make sure to select 'MTP' or 'File share' option and "
"retry.   \\n"
msgstr ""
"कोई (एमटीपी सक्रिय) एंड्रॉइड उपकरण नहीं मिला!\\n \\n यूएसबी केबल द्वारा "
"उपकरण कनेक्ट करें व \\n सुनिश्चित करें कि 'एमटीपी' या 'फाइल सहभाजन' विकल्प "
"चयनित है एवं पुनः प्रयास करें। \\n"

#: android-device-usb-connect.sh:69
msgid "EXIT"
msgstr "बंद करें"

#: android-device-usb-connect.sh:69
msgid "Retry"
msgstr "पुनः प्रयास करें"

#: android-device-usb-connect.sh:71
msgid "User pressed Exit"
msgstr "उपयोक्ता द्वारा बंद करने हेतु अनुरोध"

#: android-device-usb-connect.sh:72
msgid "User pressed Retry"
msgstr "उपयोक्ता द्वारा पुनः प्रयास हेतु अनुरोध"

#: android-device-usb-connect.sh:82
msgid "Device is mounted!"
msgstr "उपकरण माउंट है!"

#: android-device-usb-connect.sh:84
msgid "Device is NOT mounted!"
msgstr "उपकरण माउंट नहीं है!"

#: android-device-usb-connect.sh:86
msgid "Attempted to mount device and display its contents"
msgstr "उपकरण माउंट कर सामग्री प्रदर्शित करने का प्रयास जारी"

#: android-device-usb-connect.sh:91
msgid ""
"Checking if device can be mounted, asking user to grant permission on the "
"device and try to mount again"
msgstr ""
"उपकरण माउंट हेतु प्रयास जारी, उपयोक्ता से उपकरण हेतु अनुमति अनुरोध उपरांत "
"पुनः माउंट करने का प्रयास करें"

#: android-device-usb-connect.sh:93
msgid "Device seems properly mounted!"
msgstr "उपकरण उचित रूप से माउंट है!"

#: android-device-usb-connect.sh:95 android-device-usb-connect.sh:100
msgid ""
"Please check that you have ALLOWED access to your files on your android "
"device in order to proceed"
msgstr ""
"प्रक्रिया जारी रखने से पूर्व सुनिश्चित करें कि एंड्रॉइड उपकरण की फाइलों हेतु"
" अभिगम स्वीकृत है"

#: android-device-usb-connect.sh:95
msgid ""
"Please check that you have ALLOWED access to your files on your android "
"device in order to proceed\\n \\n Note: If you did not allow access, simply "
"unplug, allow permission, and plug in your device's USB cable once more"
msgstr ""
"प्रक्रिया जारी रखने से पूर्व सुनिश्चित करें कि एंड्रॉइड उपकरण की फाइलों हेतु"
" अभिगम स्वीकृत है\\n \\n ध्यान दें : अभिगम स्वीकृत न होने कि स्थिति में "
"उपकरण हटाकर अनुमति स्वीकृत करें, व यूएसबी केबल पुनः लगाएँ"

#: android-device-usb-connect.sh:98
msgid ""
"Final check to see if device can be mounted. If not, unmount it to avoid any"
" errors"
msgstr ""
"उपकरण माउंट हेतु अंतिम जाँच। विफल होने पर उपकरण हटा दें ताकि त्रुटि न हो"

#: android-device-usb-connect.sh:99
msgid "The device seems to be correctly mounted."
msgstr "उपकरण उचित रूप से माउंट है।"

#: android-device-usb-connect.sh:100
msgid ""
" Unable to mount device! \\n Please check you correctly selected 'MTP...' or"
" 'File transfer...' option.\\n Or 'Allowed' file access.\\n \\n Unplug, "
"allow permission, and plug in your device and try again."
msgstr ""
"उपकरण माउंट से हटाना विफल! \\n सुनिश्चित करें कि 'एमटीपी...' या 'फाइल "
"सहभाजन...' विकल्प चयनित है।\\n या फाइल अभिगम 'स्वीकृत' है।\\n \\n  उपकरण "
"हटाकर अनुमति स्वीकृत कर उपकरण पुनः लगाकर प्रयास करें।"
